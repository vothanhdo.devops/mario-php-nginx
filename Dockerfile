FROM ubuntu:20.04
RUN mkdir -p /var/www/mario
ENV TZ=Asia/Ho_Chi_Minh
RUN set -x \
    && ln -snf /usr/share/zoneinfo/$TZ /etc/localtime \
    && echo $TZ > /etc/timezone
WORKDIR /var/www/mario
RUN apt-get update -y
RUN apt-get install curl -y && \
    apt-get install nginx -y && \
    apt-get install software-properties-common -y && \
    add-apt-repository ppa:ondrej/php && \
    apt-get install php8.1 -y && \
    apt-get install php8.1-fpm -y
COPY ./mario.conf /etc/nginx/sites-available/default
COPY ./php-super-mario /var/www/mario
EXPOSE 8080
ADD start.sh /usr/local/bin/start.sh
RUN chmod 755 /usr/local/bin/start.sh
CMD bash -C '/usr/local/bin/start.sh';'bash'